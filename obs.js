"use strict";

const OBSWebSocket = require('obs-websocket-js');

const obs = new OBSWebSocket();

exports.on = async (eventName, eventParams) => {
    return obs.on(eventName, eventParams);
};

exports.send = async (requestName, requestFields) => {
	return obs.send(requestName, requestFields);
};

exports.connect = async () => {
    try {
        await obs.connect({ address: process.env.OBS_ADDRESS });
		console.log('OBS connected ' + process.env.OBS_ADDRESS);;
    } catch (error) {
        console.log(error);
    }
};