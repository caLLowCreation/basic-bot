require('dotenv').config({ path: __dirname + '/./.env' });
const fs = require("fs");
const tmi = require('tmi.js');
const obs = require('./obs');

const client = new tmi.client({
	identity: {
		username: process.env.BOT_USERNAME,
		password: process.env.OAUTH_TOKEN
	},
	channels: [
		process.env.CHANNEL_NAME
	]
});

const modulepath = './commands';
const commands = {};

if (module === require.main) {

	fs.readdirSync(require('path').join(__dirname, modulepath)).forEach(file => {
		const mod = require(`${modulepath}/${file}`);
		commands[file.replace('.js', '')] = mod;
	});

	client.on('message', onMessageHandler);
	client.on('connected', onConnectedHandler);
	client.connect();

	obs.on('error', onOBSErrorHandler);
	obs.connect();
}

async function onMessageHandler(channel, user, message, self) {
	if (self) return;
	if (!message.startsWith(process.env.COMMAND_PREFIX)) return;

	const parts = message.trim().split(' ');
	const command = parts.shift().replace(process.env.COMMAND_PREFIX, '');
	const params = parts;

	if (commands.hasOwnProperty(command)) {
		const result = await commands[command]({ app: { obs, client }, args: { channel, user, message, command, params } })
			.catch(e => e);

		console.log(`* Executed ${command} command`, result || 'No result');
	} else {
		console.log(`* Unknown command ${command}`);
	}
}

function onConnectedHandler(addr, port) {
	console.log(`* Connected to ${addr}:${port}`);
}

function onOBSErrorHandler(err) {
	console.error('* OBS socket error:', err);
}

