"use strict";

module.exports = ({ app: { obs, client }, args: { channel, params }}) => {
	return obs.send('GetSceneItemProperties', {
		item: params.join(' ')
	}).then(({ visible }) => Promise.all([
		obs.send('SetSceneItemProperties', { item: params.join(' '), visible: !visible }),
		client.say(channel, `You toggle to ${!visible}`)
	]));
};
